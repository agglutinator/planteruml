#!/usr/bin/env python3

import argparse
import base64
import hashlib
import shelve
import subprocess
import sys
import time
import zipfile

from pathlib import Path

from PyQt6.QtWidgets import QMainWindow, QApplication, QLabel, QFileDialog, \
    QScrollArea, QWidget, QVBoxLayout
from PyQt6.QtGui import QAction, QIcon, QPixmap
from PyQt6.QtCore import QSize, QObject, pyqtSignal, QRunnable, QThreadPool, Qt

RENDER_JAR_KEY = "render_jar"
DIAGRAM_KEY = "diagram"
STATE_FILE = "planter_state"
GEOMETRY_KEY = "geometry"


class DiagramLabel(QLabel):
    got_file_path = pyqtSignal(str)

    def __init__(self, *args, **kwargs):
        QLabel.__init__(self, *args, **kwargs)
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasText():
            event.acceptProposedAction()

    def dropEvent(self, event):
        text = event.mimeData().text()
        event.acceptProposedAction()
        self.got_file_path.emit(text)


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self._state = None
        self._state_file_path = STATE_FILE
        self._renderer_jar_file = None
        self._sequence_uml_file = None
        self._img = None
        self.setWindowTitle("Planter!UML")
        # self.setFixedSize(QSize(1024, 768))

        self._label = DiagramLabel(alignment=Qt.AlignmentFlag.AlignTop)
        self._label.got_file_path.connect(self.set_file_target)
        self._layout = QVBoxLayout()
        self._scroll = QScrollArea()

        self._create_actions()
        # self._create_menu()

        self._widget = QWidget()
        self._widget.setLayout(self._layout)
        self._layout.addWidget(self._label)

        # self._layout = QVBoxLayout(widget)
        # self.setWidgetResizable(True)

        self._scroll.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAsNeeded)
        self._scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAsNeeded)
        self._scroll.setWidgetResizable(True)
        self._scroll.setWidget(self._widget)
        # self.setWidget(widget)
        self.setCentralWidget(self._scroll)

        # self.setCentralWidget()
        self.threadpool = QThreadPool()
        self._worker = Worker(self._renderer_jar_file, self._sequence_uml_file)
        self._worker.signals.render.connect(self.show_picture)
        self.threadpool.start(self._worker)
        self._restore_state(self._state_file_path)
        self.show()

    @property
    def renderer_jar_file(self):
        return self._renderer_jar_file

    @renderer_jar_file.setter
    def renderer_jar_file(self, value: Path):
        self._renderer_jar_file = value
        if self._worker:
            self._worker.renderer = value

    @property
    def sequence_uml_file(self):
        return self._sequence_uml_file

    @sequence_uml_file.setter
    def sequence_uml_file(self, value: Path):
        self._sequence_uml_file = value
        if self._worker:
            self._worker.sequence_diagram = value
        window_title = f"Planter!UML - {str(self._sequence_uml_file)}"
        self.setWindowTitle(window_title)


    def _restore_state(self, file_path):
        self._state = shelve.open(file_path)
        if RENDER_JAR_KEY in self._state:
            render_path = self._state[RENDER_JAR_KEY]
            print(f"read value: [{render_path}]")
            self.renderer_jar_file = Path(render_path)
        if DIAGRAM_KEY in self._state:
            diagram_path = self._state[DIAGRAM_KEY]
            print(f"read value: [{diagram_path}]")
            self.sequence_uml_file = Path(diagram_path)
        if GEOMETRY_KEY in self._state:
            geometry = self._state[GEOMETRY_KEY]
            print(f"read geometry value")
            self.restoreGeometry(geometry)
        self._state.close()

    def _create_actions(self):
        self.openAction = QAction("&Open sequence file...", self)
        self.openAction.triggered.connect(self._open_action)
        self.openRendererAction = QAction("&Set renderer...", self)
        self.openRendererAction.triggered.connect(self._open_renderer_action)

    def _create_menu(self):
        menu = self.menuBar()
        file_menu = menu.addMenu("&File")
        file_menu.addAction(self.openRendererAction)
        file_menu.addAction(self.openAction)

    def _open_action(self, s):
        file_name = QFileDialog.getOpenFileName(self, "Open File", ".", "PlantUML sequences (*txt);; All Files(*)")
        print(f"open {file_name[0]}")
        self._sequence_uml_file = Path(file_name[0])
        self._worker.sequence_diagram = self._sequence_uml_file

    def _open_renderer_action(self, s):
        file_name = QFileDialog.getOpenFileName(self, "Open File", ".", "Java Files (*jar);; All Files(*)")
        print(f"open {file_name[0]}")
        self._renderer_jar_file = Path(file_name[0])
        self._worker.renderer = self._renderer_jar_file

    def show_picture(self):
        if self._img:
            self._img.close()
        pixmap = QPixmap(str(self._sequence_uml_file.with_suffix(".png")))
        self._label.setPixmap(pixmap)
        size = pixmap.size()
        new_size = QSize(size.width() + 16, size.height() + 16)
        # max_size_hint = self._layout.maximumSize()
        # size_hint = self._layout.sizeHint()
        total_size_hint = self._layout.totalSizeHint()
        if new_size.width() > total_size_hint.width():
            new_size.setWidth(total_size_hint.width())
        if new_size.height() > total_size_hint.height():
            new_size.setHeight(total_size_hint.height() - 64)
        # self.setFixedSize(new_size)

    def set_file_target(self, file_path: str):
        path = Path(file_path.replace("\r", "").replace("\n", "").replace("file://", ""))
        if path.exists():
            if "jar" in path.suffix:
                try:
                    zip_file = zipfile.ZipFile(str(path), 'r')
                    directory_list = zip_file.infolist()
                    self.renderer_jar_file = path
                except FileNotFoundError as e:
                    print(f"file not found: {file_path}")
                except:
                    pass
            else:
                self.sequence_uml_file = path

    def closeEvent(self, event) -> None:
        print("saving state")
        self._save_state(self._state_file_path)
        print("stopping worker")
        self._worker.stop()

    def _save_state(self, file_path):
        self._state = shelve.open(file_path)
        self._state[RENDER_JAR_KEY] = self._renderer_jar_file
        self._state[DIAGRAM_KEY] = self._sequence_uml_file
        geometry = self.saveGeometry()
        self._state[GEOMETRY_KEY] = geometry
        self._state.close()


class Worker(QRunnable):
    def __init__(self, renderer_path: Path = None, sequence_path: Path = None):
        super(Worker, self).__init__()
        self._renderer_jar_file = renderer_path
        self._sequence_uml_file = sequence_path
        self._check_sum = hashlib.md5(base64.b64encode(bytes("empty", "utf-8")))
        self._running = False
        self.signals = WorkerSignals()

    def run(self) -> None:
        print("[worker] started to run")
        self._running = True
        while self._running:
            # QtFileSystemWatcher didn't work with noatime set for drive
            if self._renderer_jar_file and self._sequence_uml_file:
                file_content = self._sequence_uml_file.read_text()
                encoded = base64.b64encode(bytes(file_content, "utf-8"))
                new_check_sum = hashlib.md5(encoded)
                if new_check_sum.digest() != self._check_sum.digest():
                    self._check_sum = new_check_sum
                    print("[worker] diagram is changed, rendering")
                    cmd = ["java", "-jar", f"{self._renderer_jar_file}",
                           f"{self._sequence_uml_file}"]
                    process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
                    output, error = process.communicate()
                    print("[worker] processed diagram")
                    self.signals.render.emit()
            time.sleep(1)

    @property
    def sequence_diagram(self, sequence_path: Path):
        return self._sequence_uml_file

    @sequence_diagram.setter
    def sequence_diagram(self, sequence_path: Path):
        print(f"[worker] got sequence diagram: [{sequence_path}]")
        self._sequence_uml_file = sequence_path

    @property
    def renderer(self, renderer_path: Path):
        return self._renderer_jar_file

    @renderer.setter
    def renderer(self, renderer_path: Path):
        print(f"[worker] got renderer: [{renderer_path}]")
        self._renderer_jar_file = renderer_path

    def stop(self):
        self._running = False


class WorkerSignals(QObject):
    render = pyqtSignal()


def main(input_params: argparse.Namespace):
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon("images/planter.png"))
    w = MainWindow()
    app.exec()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p',
                        '--path',
                        type=str,
                        required=False,
                        help='path to plantuml text file'
                        )
    parser.add_argument('-s',
                        '--sleep',
                        type=int,
                        required=False,
                        help='seconds to wait for new file content',
                        default=5
                        )

    input_params = parser.parse_args()
    main(input_params)

