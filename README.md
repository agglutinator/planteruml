# Planter!UML

A project to render standard plantuml sequence diagrams. The only two things are needed: java jar renderer from an official site and text diagram: drop them just right on the main window. Then edit the text file and the renderer will show you the changes each time you save the file.

Implemented in Python with PyQt6. Uses shelve to store state on exit.

## Dependencies ##
    PyQt6 (https://pypi.org/project/PyQt6):
      pip install PyQt6 

    Java runtime

    PlantUML Java renderer (https://plantuml.com/en/download)

## Run ##

    python planter.py

## TODO ##
1. Add scrolling for big diagrams
